package pt122.adnmanager.library;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;
import static test.AdnTester.testCorrecteADN;

/**
 *
 * @author tarda
 */
public class ManagerTests {
    
    public ManagerTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testDemoOK() {
        String resEsperat = "AA";
        String resReal = "AA";
        assertEquals(resEsperat, resReal);
    }
    
    @Test
    public void testADNValido() {
        ManagerADN managerADN = new ManagerADN();
        // Hem de testejar les següents funcions.
        boolean resEsperat = true;
        //    • El mètode que valida la cadena d’ADN
        assertEquals(managerADN.EsCorrecteADN("AA"), resEsperat);
    }
    
    @Test
    public void testADNNoValido() {
        ManagerADN managerADN = new ManagerADN();
        // Hem de testejar les següents funcions.
        boolean resEsperat = false;
        //    • El mètode que valida la cadena d’ADN
        assertEquals(managerADN.EsCorrecteADN("ALAI"), resEsperat);
    }
    
    @Test
    public void testADNLargoValido() {
        ManagerADN managerADN = new ManagerADN();
        // Hem de testejar les següents funcions.
        boolean resEsperat = false;
        //    • El mètode que valida la cadena d’ADN
        assertEquals(managerADN.EsCorrecteADN("ACGTCATCGAAATGGAAGTE"), resEsperat);
    }
    
    @Test
    public void TestDonarVoltaADN(){
    
     ManagerADN managerADN = new ManagerADN();
        // Hem de testejar les següents funcions.
       String resEsperat = "AGTCAGTC";
       String Respuesta = "CTGACTGA";
        //    • El mètode que valida la cadena d’ADN
        
        assertEquals(new String(managerADN.donarVoltaADN("CTGACTGA")), resEsperat);
    }

     @Test
    public void TestBaseMasRepetida(){
    
     ManagerADN managerADN = new ManagerADN();
        // Hem de testejar les següents funcions.
       String ADN = "AGTCAGTCGTACGTAAAA";
       String Respuesta = "A, repetida 7 veces";
        //    • El mètode que valida la cadena d’ADN
        
        assertEquals(managerADN.baseMesRepetida(ADN), Respuesta);
    }
    
    
     @Test
    public void TestADNtoARN(){
    
     ManagerADN managerADN = new ManagerADN();
        // Hem de testejar les següents funcions.
       String ADN = "AGTCAGTCGTACGTAAAA";
       String Respuesta = "AGUCAGUCGUACGUAAAA";
        //    • El mètode que valida la cadena d’ADN
        assertEquals(managerADN.ADNtoARN(ADN), Respuesta);
    }   
}


package test;

import pt122.adnmanager.library.ManagerADN;

/**
 *
 * @author tarda
 */
public class AdnTester {

    public static void main(String[] args) {
        ManagerADN managerADN = new ManagerADN();
        // Hem de testejar les següents funcions.
        
        //    • El mètode que valida la cadena d’ADN
        testCorrecteADN(managerADN,"AGATTAACGAA");
        testCorrecteADN(managerADN,"ACGAYUL");
        
//    • La base més repetida.
//    • La base menys repetida.
//    • El mètode que converteix l’ADN a ARN.
    }
    
    public static void testCorrecteADN(ManagerADN managerADN, String testADN) {
        System.out.println(testADN + ", correcte ? ");
        System.out.println(managerADN.EsCorrecteADN(testADN));
    }
   
                            
}

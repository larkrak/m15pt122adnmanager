/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt122.adnmanager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import pt122.adnmanager.library.ADNFileReader;
import pt122.adnmanager.library.ManagerADN;
import pt122.adnmanager.library.ADNManagerMenu;

/**
 *
 * @author mique
 */
public class ProgramaADN {



    public static void main(String[] args) {

        // 1. Mètode llegir fitxer amb una cadena ADN.
        ArrayList ADNFitxer = ADNFileReader.llegeixFitxerADN("ADN.txt");
        Integer pos = 0;
        Integer incorrectas = 0;
        // 2. Validar l'ADN del fitxer
        Iterator i = ADNFitxer.iterator();
        while (i.hasNext()) {
            if (ManagerADN.EsCorrecteADN(i.next().toString())) {
                System.out.println(ConsoleColors.GREEN + "La cadena: " + ADNFitxer.get(pos) + " es correcta");
            } else {
                System.out.println(ConsoleColors.RED + "La cadena: " + ADNFitxer.get(pos) + " es incorrecta");
                incorrectas += 1;
            }

            pos += 1;
        }

        // 3a. Si és vàlid, ja podem mostrar el menu
        if (incorrectas == 0) {
            ADNManagerMenu menu = new ADNManagerMenu();
            menu.run();
        }else{
            if(incorrectas == 1){
                System.out.println(ConsoleColors.RED + incorrectas+" cadena es incorrecta");
            }else{
                System.out.println(ConsoleColors.RED_BOLD + incorrectas+" cadenas son incorrecta");
            }
        }

        // 3b. Sortim del programa si l'ADN no es valid.
    }
}

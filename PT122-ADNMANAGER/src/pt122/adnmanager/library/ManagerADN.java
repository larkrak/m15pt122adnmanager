package pt122.adnmanager.library;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

/**
 * Llibreria que conté mètodes per a tractar cadenes d'ADN.
 *
 * @author mique
 */
public class ManagerADN {

    private int ContA;
    private int ContT;
    private int ContG;
    private int ContC;

    public ManagerADN() {
        ContA = 0;
        ContT = 0;
        ContG = 0;
        ContC = 0;
    }

    //- Donar la volta a l’ADN. (retorn String)
    /**
     *
     * @param ADN
     * @return True si es correcte, False si no es un ADN valid.
     */
    public static boolean EsCorrecteADN(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        char[] caractersADN = ADN.toCharArray();
        boolean result = true;
        Integer size = caractersADN.length;
        Integer ContA = 0;
        Integer ContT = 0;
        Integer ContG = 0;
        Integer ContC = 0;

        for (char c : caractersADN) {
            switch (c) {
                case 'A':
                    ContA++;
                    break;
                case 'T':
                    ContT++;
                    break;
                case 'G':
                    ContG++;
                    break;
                case 'C':
                    ContC++;
                    break;
                default:
                    break;
            }
        }

        if ((ContA + ContC + ContG + ContT) != size) {
            result = false;
        }

        return result;
    }

    /**
     * Retorna la cadena ADN passada per paràmtre invertida.
     *
     * @param ADN Cadena ADN.
     * @return Cadena ADN Invertida
     */
    public static char[] donarVoltaADN(String ADN) {

        char[] adn = ADN.toCharArray();
        char[] ret = new char[adn.length];

        for (int i = 0; i != adn.length; i++) {
            ret[i] = adn[((adn.length) - 1) - i];
        }

        //System.out.println(ret);
        return ret;
    }

//- Trobar la base més repetida. (retorn caràcter)
    public static String baseMesRepetida(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.

        char[] caractersADN = ADN.toCharArray();
        ArrayList<Integer> arr = new ArrayList<Integer>();
        Hashtable<String, Integer> bases = new Hashtable<String, Integer>();
        Integer ContA = 0;
        Integer ContT = 0;
        Integer ContG = 0;
        Integer ContC = 0;
        String basemesrepetida = "";

        for (char c : caractersADN) {
            switch (c) {
                case 'A':
                    ContA++;
                    break;
                case 'C':
                    ContC++;
                    break;
                case 'T':
                    ContT++;
                    break;
                case 'G':
                    ContG++;
                    break;
                default:
                    break;
            }
        }
        
        arr.add(ContA);
        arr.add(ContC);
        arr.add(ContT);
        arr.add(ContG);
        
        Integer max = Collections.max(arr);
        
        bases.put("A", ContA);
        bases.put("C", ContC);
        bases.put("T", ContT);
        bases.put("G", ContG);
        
        for (Map.Entry<String,Integer> entry : bases.entrySet()) {
            
            if(entry.getValue() == max){
                basemesrepetida += entry.getKey() + ", repetida "+entry.getValue()+" veces\n";
            }
        }
        
                
        return basemesrepetida;
    }

//- Trobar la base menys repetida. (retorn caràcter)
    public static String baseMenysRepetida(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        char[] caractersADN = ADN.toCharArray();
        ArrayList<Integer> arr = new ArrayList<Integer>();
        Hashtable<String, Integer> bases = new Hashtable<String, Integer>();
        Integer ContA = 0;
        Integer ContT = 0;
        Integer ContG = 0;
        Integer ContC = 0;
        String basemesrepetida = "";

        for (char c : caractersADN) {
            switch (c) {
                case 'A':
                    ContA++;
                    break;
                case 'C':
                    ContC++;
                    break;
                case 'T':
                    ContT++;
                    break;
                case 'G':
                    ContG++;
                    break;
                default:
                    break;
            }
        }
        if(ContA != 0) arr.add(ContA);
        if(ContC != 0)arr.add(ContC);
        if(ContT != 0)arr.add(ContT);
        if(ContG != 0)arr.add(ContG);
        
        Integer min = Collections.min(arr);
        
        bases.put("A", ContA);
        bases.put("C", ContC);
        bases.put("T", ContT);
        bases.put("G", ContG);
        
        for (Map.Entry<String,Integer> entry : bases.entrySet()) {
            
            if(entry.getValue() == min){
                basemesrepetida += entry.getKey() + ", repetida "+entry.getValue()+" veces\n";
            }
        }         
        return basemesrepetida;
    }

    //- Fer recompte de bases i mostrar-lo. (1*)ç
    public static String recompteBases(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.

        char[] caractersADN = ADN.toCharArray();
        Integer ContA = 0;
        Integer ContT = 0;
        Integer ContG = 0;
        Integer ContC = 0;

        for (char c : caractersADN) {
            switch (c) {
                case 'A':
                    ContA++;
                    break;
                case 'C':
                    ContC++;
                    break;
                case 'T':
                    ContT++;
                    break;
                case 'G':
                    ContG++;
                    break;
                default:
                    break;
            }
        }
        String TOTAL = "Total de ";
        StringBuilder sb = new StringBuilder();
        sb.append(TOTAL);
        sb.append(" A = ");
        sb.append(ContA);
        sb.append("\n");
        sb.append(TOTAL);
        sb.append(" C = ");
        sb.append(ContC);
        sb.append("\n");
        sb.append(TOTAL);
        sb.append(" G = ");
        sb.append(ContG);
        sb.append("\n");
        sb.append(TOTAL);
        sb.append(" T = ");
        sb.append(ContT);
        return sb.toString();
    }

//- Convertir l’ADN a ARN (2*)
    public static String ADNtoARN(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        
        char[] caractersADN = ADN.toCharArray();
        String ret;
        
        for (int i = 0; i < caractersADN.length; i++) {
            
            if(caractersADN[i] == 'T'){
                caractersADN[i] = 'U';
            }
        }
        
        ret = String.valueOf(caractersADN);
        
        return ret;
    }

//- Número de vegades que apareix un tros de cadena ADN. (3*)
//    https://gitlab.com/dawbio2-m15-uf1-a01-2020/provant-exp-regulars/-/blob/master/ProvaExpRegulars/src/provaexpregulars/ExpRegularsADN.java
//    // Contar cuantas veces aparece en un ADN la cadena "cat" o "gat"
//       ADNcadena = "catagacatcat";
//       System.out.println("cat cuantas veces aparece en el ADN catagacatcat? "); 
//       pat = Pattern.compile("cat");
//       mat = pat.matcher(ADNcadena);
//       int cont = 0;
//       while (mat.find()) {
//           cont++;
//       }
//- Sortir.
}

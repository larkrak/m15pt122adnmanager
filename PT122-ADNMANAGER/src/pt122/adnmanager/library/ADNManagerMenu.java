/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt122.adnmanager.library;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import pt122.adnmanager.ConsoleColors;

/**
 *
 * @author ismac
 */
public class ADNManagerMenu {
    
    private String[] menuOptions
            = {"Sortir",
                "Donar la volta a l’ADN. (retorn String)",
                "Trobar la base més repetida. (retorn caràcter)",
                "Trobar la base menys repetida. (retorn caràcter)",
                "Fer recompte de totes les bases",
                "Convertir l’ADN a ARN (2*)",
                "Número de vegades que apareix un tros de cadena ADN. (3*)"
            };

    private int showMenu() {
        int option = -1;
        System.out.println("Menu");
        for (int i = 0; i < menuOptions.length; i++) {
            // System.out.println(i + ")"+ menuOptions[i]);
            //  String texto = String.format("%d)%s", i, menuOptions[i]);
            // System.out.println(texto);
            System.out.println(String.format(
                    "%d)%s", i, menuOptions[i]));
        }
        System.out.print("Choose your option:");
        Scanner lector = new Scanner(System.in);
        try {
            option = lector.nextInt();
        } catch (Exception e) {
            System.out.println("Option not valid");
        }

        return option;

    }

    public void run() {

        //  loadDAta();
        int option = -1;
        do {
            option = showMenu();
            ArrayList ADNFitxer = ADNFileReader.llegeixFitxerADN("ADN.txt");
            Iterator i = ADNFitxer.iterator();
            Integer pos = 0;
            switch (option) {
                
                case 1:
                    System.out.println("Donar la volta a l’ADN. (retorn String)");
                    while (i.hasNext()) {
                        if (ManagerADN.EsCorrecteADN(i.next().toString())) {
                System.out.println(ConsoleColors.GREEN + "La cadena: " + ADNFitxer.get(pos) + " es correcta");
                System.out.println("La cadena -> "+ADNFitxer.get(pos)+"<- quedaria de la siguiente manera:");
                        System.out.println(ManagerADN.donarVoltaADN(i.next().toString()));
            } else {
                System.out.println(ConsoleColors.RED + "La cadena: " + ADNFitxer.get(pos) + " es incorrecta");
                System.exit(0);
            }
                        
                        pos++;
                    }
                    break;
                case 2:
                    System.out.println("Trobar la base més repetida. (retorn caràcter)");
                    while (i.hasNext()) {
                        System.out.println("La base mas repetida de -> "+ADNFitxer.get(pos)+"<- es la siguiente:");
                        System.out.println(ManagerADN.baseMesRepetida(i.next().toString()));
                        pos++;
                    }

                    break;
                case 3:
                    System.out.println("Trobar la base menys repetida. (retorn caràcter)");
                    while (i.hasNext()) {
                        System.out.println("La base menos repetida de -> "+ADNFitxer.get(pos)+"<- es la siguiente:");
                        System.out.println(ManagerADN.baseMenysRepetida(i.next().toString()));
                        pos++;
                    }

                    break;
                case 4:
                    System.out.println("Fer recompte de totes les bases");
                    while (i.hasNext()) {
                        System.out.println("La cadena -> "+ADNFitxer.get(pos)+"<- quedaria de la siguiente manera:");
                        System.out.println(ManagerADN.recompteBases(i.next().toString()));
                        pos++;
                    }
                    
                    break;
                case 5:
                    System.out.println("Convertir l’ADN a ARN (2*)");
                    while (i.hasNext()) {
                        System.out.println("La cadena -> "+ADNFitxer.get(pos)+"<- a ARN quedaria de la siguiente manera:");
                        System.out.println(ManagerADN.ADNtoARN(i.next().toString()));
                        pos++;
                    }

                    break;
                case 6:
                    System.out.println("Número de vegades que apareix un tros de cadena ADN. (3*)");

                    break;

            }

        } while (option != 0);
        System.out.println("You have choosen the option " + option);
    }
    
    
}
